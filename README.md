# Login Service

May First login service API. The API is used to validate username/password
combinations.

## Launch

Edit `loginservice.yml` to set configuration details. This file will be sought
in the following locations:

 * `/etc/loginservice.yml`
 * The working directory.

Launch with: `gunicorn loginservice:app`

## Usage

The API has three end points:

 * `/check`: test a username and password
 * `/user`: test a username
 * `/clear`: clear the cache (necessary when a username or password has changed)

The API expects the following inputs:

 * `app_id`: all requests must come with a valid APP ID to ensure the app is authorized to use this service
 * `user`: the username to test
 * `password`: the password to test
 * `output`: the output method, defaults to text, json is also available.

The API prefers POST but will accept GET requests as well.

The API will respond with a 200 response code and the message "yes" if the
request is successful, a 406 response code and an error message if something is
wrong, a 404 if we do not recognize the endpoint, or a 403 message and no
output if the username or password are wrong.

Applications should check for the "yes" message to ensure a user is validated.

If the output is text (default for backward compatibility), then only the
message is returned. If the output is json, then the message will be formatted
along these lines:

```
{
  "is_error": 0,
  "message": "message goes here"
}
```

A successful response will have `is_error` set to 0, an error will have it set
to 1.

## Testing

You can test with curl, e.g.:

    curl -d app_id=1234 -d user=admin -d password=admin23 http://localhost:8000/check

"""
Basic API for validating users and user and passwords.

All requests must come with a shared secret App ID to avoid casual user
or password testing.

Valid users and passwords are cached in memory - call the /clear end point
to clear them.
"""

import os
import logging
from passlib.context import CryptContext
from passlib.hash import sha512_crypt, md5_crypt
import falcon
import pymysql
import yaml

class sha512_crypt_plus(sha512_crypt):
    """
    For backward compatibility, we extend the sha512_crypt library to
    support the "+" character in salts because the control panel used
    salts with plus characters through about May 2024.
    """
    salt_chars = sha512_crypt.salt_chars + '+'

class LoginServiceHelper:
    """
    Helper class for accessing the database, setting up
    crypt variables and verifying users and passwords.
    """
    conf = {}
    OUTPUT_TEXT = 'text'
    OUTPUT_JSON = 'json'
    YES = 'yes'
    default_output = ''
    database = None
    ready = False
    app_ids = {}
    cache = {}
    # passlib CryptContext object.
    crypt = None

    def load(self):
        """
        Initialize the helper class.
        """
        # Load configuration file.
        conf_files = ['loginservice.yml', '/etc/loginservice.yml']
        for conf_file in conf_files:
            if os.path.exists(conf_file):
                with open(conf_file, encoding='utf8') as file_desc:
                    self.conf = yaml.load(file_desc, Loader=yaml.FullLoader)
        if not self.conf:
            print(f"Failed to find configuration file. Tried {conf_files}.", flush=True)
            return False

        # Setup logging.
        loglevel = logging.WARNING
        if "debug" in self.conf and self.conf["debug"] is True:
            loglevel = logging.DEBUG
        logging.basicConfig(
            format='[%(asctime)s] [falcon] [%(levelname)s] %(message)s',
            datefmt='%Y-%m-%d %H:%M:%S %z',
            level=loglevel
        )

        if not self.initialize_database():
            return False

        self.cache = {
            "user": {},
            "password": {}
        }

        # Create a cryptcontext that includes a "category" called
        # "md5" that means we want an md5 hash, not the default
        # sha512 hash.
        self.crypt = CryptContext(
            schemes=["sha512_crypt", "md5_crypt"],
            md5__md5_crypt=None,
        )

        # Set to text for backwards compatibility.
        self.default_output = self.OUTPUT_TEXT
        self.ready = True
        return True

    def initialize_database(self):
        """
        Setup the database connection and cursos.
        """
        logging.info("Initializing database")
        try:
            if "db_host" in self.conf:
                self.database = pymysql.connect(
                    host=self.conf["db_host"],
                    user=self.conf["db_user"],
                    password=self.conf["db_password"],
                    database=self.conf["db_name"]
                )
            elif "db_unix_socket" in self.conf:
                self.database = pymysql.connect(
                    unix_socket=self.conf["db_unix_socket"],
                    user=self.conf["db_user"],
                    password=self.conf["db_password"],
                    database=self.conf["db_name"]
                )
            else:
                self.database = pymysql.connect(
                    user=self.conf["db_user"],
                    password=self.conf["db_password"],
                    database=self.conf["db_name"]
                )
        except KeyError:
            msg = (
                "Please include db_host (or db_unix_socket), db_user, db_password ",
                "and db_name in the config file."
            )
            logging.critical(msg)
            return False
        return True

    def query(self, sql, params, retry=0):
        # Note: we have to intialize the db on every query to ensure we
        # get a fresh result from the mysql methods.
        self.initialize_database()
        with  self.database.cursor() as cursor:
            cursor.execute(sql, params)
            return cursor

    def validate_app_id(self, app_id):
        """
        Check of app id is valid.
        """

        if app_id in self.conf["app_ids"]:
            return True
        return False

    def validate_user(self, user):
        """
        Check if user is valid.
        """

        if not user:
            return False
        if user in self.cache["user"]:
            logging.debug("Found %s in user cache.", user)
            return True
        logging.debug("Did not find %s in user cache.", user)
        sql = "SELECT get_salt(%s)"
        cursor = self.query(sql, (user,))
        salt = cursor.fetchone()
        if salt and salt[0]:
            salt = salt[0]
        else:
            return False
        self.cache["user"][user] = True
        return True

    def validate_user_password(self, user, password):
        """
        Check if user and password are valid.
        """

        if not password:
            return False
        # Check for hash in memory for faster response.
        if user in self.cache["password"]:
            logging.debug("Found %s in password cache", user)
            if self.crypt.verify(password, self.cache["password"][user]):
                logging.debug(
                    "User %s password matches cache entry.",
                    user
                )
                return True

        logging.debug("Did not find valid password for %s in password cache.", user)
        # Get the salt so we can generate a hash.
        sql = "SELECT get_salt(%s)"
        cursor = self.query(sql, (user,))
        salt = cursor.fetchone()
        if salt and salt[0]:
            salt = salt[0]
        else:
            logging.debug("Did not find valid salt for %s.", user)
            return False
        return self.query_for_valid_hash(password, salt, user)

    def query_for_valid_hash(self, password, salt, user):
        """
        Call SQL to test valid hash.
        """

        parts = salt.split('$')
        try:
            scheme = parts[1]
            if scheme == "1":
                salt_part = parts[2]
                logging.debug("Using md5 salt")
                hashed = md5_crypt.using(salt=salt_part).hash(password)
            elif scheme == "6":
                rounds_part = parts[2]
                salt_part = parts[3]
                rounds_parts = rounds_part.split('=')
                rounds = int(rounds_parts[1])
                logging.debug("Using sha5-512 salt with %s rounds", rounds)
                hashed = sha512_crypt_plus.using(salt=salt_part, rounds=rounds).hash(password)
            else:
                logging.error("Unknown scheme in salt %s", salt)
                return False
        except (KeyError, IndexError, ValueError) as error:
            logging.error("Unable to parse salt: %s. Error %s", salt, error)
            return False

        sql = "SELECT valid_hash(%s,%s) AS hash"
        cursor = self.query(sql, (hashed, user))
        response = cursor.fetchone()
        if response and response[0]:
            response = response[0]
        if response != 1:
            logging.debug("Failed to find valid hash using %s and %s.", hashed, user)
            return False
        # Our legacy systems have some passwords with plus signs. If we add
        # a hash with a plus sign, we will get an error when we try to verify
        # it, so generate a new hash for the cache.
        if '+' in hashed:
            hashed = sha512_crypt.hash(password)
        self.cache["password"][user] = hashed
        return True

    def set_content_type(self, resp, user_output):
        """
        Set content type, either default or based on request.
        """

        valid_output = True
        output = user_output
        if not output:
            output = self.default_output
        if output not in (self.OUTPUT_TEXT, self.OUTPUT_JSON):
            output = self.default_output
            valid_output = False

        if output == self.OUTPUT_TEXT:
            resp.content_type = falcon.MEDIA_TEXT
        elif output == self.OUTPUT_JSON:
            resp.content_type = falcon.MEDIA_JSON

        if valid_output:
            return True

        # Invalid output. Return an error.
        resp.status = falcon.HTTP_406
        self.format_output(resp, f'Unknown output: {user_output}')
        return False

    def format_output(self, resp, message = ""):
        """
        Output message based on chosen output format.
        """

        if resp.content_type == falcon.MEDIA_TEXT:
            # With text output we just print out the message.
            resp.text = message
        elif resp.content_type == falcon.MEDIA_JSON:
            # Output depends on whether it's an error or not.
            if resp.status == falcon.HTTP_403:
                # No output on access denied, just the 403
                resp.media = {}
            elif resp.status == falcon.HTTP_200:
                resp.media = {
                    "is_error": 0,
                    "message": message
                }
            else:
                # It's an error, show error.
                resp.media = {
                    "is_error": 1,
                    "message": message
                }


class LoginService:
    """
    The API service provided via the falcon framework.
    """
    clear_user = ''
    user = ''
    password = ''
    app_id = ''
    output = ''

    def on_get_user_and_password(self, req, resp):
        """
        Handler for get requests to check user and password.
        """

        self.on_request_user_and_password(req, resp)

    def on_get_user(self, req, resp):
        """
        Handler for get request to check user.
        """

        self.on_request_user(req, resp)

    def on_post_user_and_password(self, req, resp):
        """
        Handler for post request to check user and password.
        """

        self.on_request_user_and_password(req, resp)

    def on_post_user(self, req, resp):
        """
        Handler for post request to check user.
        """

        self.on_request_user(req, resp)

    def on_post_clear(self, req, resp):
        """
        Handler for post request to clear user or password
        from cache.
        """

        logging.debug("check request")
        if self.check_request(req, resp) is False:
            return
        try:
            if self.clear_user:
                logging.debug("Clearing user %s", self.clear_user)
                del helper.cache["user"][self.clear_user]
                del helper.cache["password"][self.clear_user]
            else:
                logging.error("Clear called without passing user to clear.")

        except KeyError:
            pass

    def on_request_user_and_password(self, req, resp):
        """
        Test user and password from either get or post request.
        """

        if self.check_request(req, resp) is False:
            return
        if self.check_user(resp) is False:
            return
        self.check_password(resp)

    def on_request_user(self, req, resp):
        """
        Test user for either get or post request.
        """

        if self.check_request(req, resp) is False:
            return
        self.check_user(resp)

    def check_user(self, resp):
        """
        Check for valid user, handle http response.
        """

        if helper.validate_user(self.user) is False:
            resp.status = falcon.HTTP_403
            helper.format_output(resp)
            logging.warning(
                "Invalid user %s from app %s",
                self.user,
                helper.conf['app_ids'][self.app_id]
            )
            return False

        logging.debug(
            "Valid user %s from app %s.",
            self.user,
            helper.conf['app_ids'][self.app_id]
        )
        resp.status = falcon.HTTP_200
        helper.format_output(resp, helper.YES)
        return True

    def check_password(self, resp):
        """
        Check for valid password, handle http response.
        """

        if helper.validate_user_password(self.user, self.password) is False:
            resp.status = falcon.HTTP_403
            helper.format_output(resp)
            logging.warning(
                "Invalid password for %s from app %s.",
                self.user,
                helper.conf['app_ids'][self.app_id]
            )
            return False

        logging.debug(
            "Valid password for %s from app %s.",
            self.user,
            helper.conf['app_ids'][self.app_id]
        )
        resp.status = falcon.HTTP_200
        helper.format_output(resp, helper.YES)
        return True

    def check_request(self, req, resp):
        """
        Check general validty of request, handle http response.
        """

        if req.method == 'GET':
            self.app_id = req.get_param('app_id')
            self.user = req.get_param('user')
            self.clear_user = req.get_param('clear_user')
            self.password = req.get_param('password')
            self.output = req.get_param('output')
        else:
            media = req.get_media()
            attributes = [
                "app_id",
                "user",
                "password",
                "output",
                "clear_user",
            ]
            for attribute in attributes:
                if attribute in media:
                    setattr(self, attribute, media[attribute])

        if helper.set_content_type(resp, self.output) is False:
            return False

        if helper.ready is False:
            resp.status = falcon.HTTP_406
            helper.format_output(resp, 'App is not ready, contact administrator.')
            return False

        if helper.validate_app_id(self.app_id) is False:
            resp.status = falcon.HTTP_403
            helper.format_output(resp)
            logging.warning(
                "Invalid app id (%s) from %s.",
                self.app_id,
                req.remote_addr
            )
            return False

        return True

# Load the helper app once.
helper = LoginServiceHelper()
helper.load()

app = falcon.App()
app.add_route('/check', LoginService(), suffix="user_and_password")
app.add_route('/user', LoginService(), suffix="user")
app.add_route('/clear', LoginService(), suffix="clear")
